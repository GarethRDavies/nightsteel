﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeIn : MonoBehaviour {

    public float fadeInTime;

    private Image fadePanel;
    private Color currentColour = Color.black;
    private static bool doFade = true;                                                      //So can control the fade (only fades once, and not from options).

	// Use this for initialization
	void Start () {
        fadePanel = GetComponent<Image>();
	}
	
	// Update is called once per frame
	void Update () {
        if (doFade)
        {
            if (Time.timeSinceLevelLoad < fadeInTime)
            {
                //fade in
                float alphaChange = Time.deltaTime / fadeInTime;
                currentColour.a -= alphaChange;
                fadePanel.color = currentColour;
            }
            else
            {
                doFade = false;
                //deactivate panel
                gameObject.SetActive(false);
            }
        }
        else
        {
            gameObject.SetActive(false);
        }
	}
}
