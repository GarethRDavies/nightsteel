﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MusicManager : MonoBehaviour {

    public AudioClip[] levelMusicChangeArray;

    private AudioSource audioSource;

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    // Use this for initialization
    void Start () {
        audioSource = GetComponent<AudioSource>();
        SceneManager.sceneLoaded += OnLevelLoaded;
        ChangeVolume(PlayerPrefsManager.GetMasterVolume());                                                                         //Make sure the volume is set to what is in player prefs from the start
	}
	
	void OnLevelLoaded(Scene scene, LoadSceneMode mode)
    {
        if (levelMusicChangeArray[scene.buildIndex] == null || levelMusicChangeArray[scene.buildIndex] == audioSource.clip)
        {
            return;
        }

        Debug.Log("Playing clip " + levelMusicChangeArray[scene.buildIndex]);
        AudioClip audioClip = levelMusicChangeArray[scene.buildIndex];

        if (audioClip)
        {
            audioSource.clip = audioClip;
            audioSource.loop = true;
            audioSource.Play();
        }
    }

    public void ChangeVolume(float volume)
    {
        audioSource.volume = volume;
    } 
}
